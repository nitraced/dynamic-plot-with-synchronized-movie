
#!/bin/bash
# Bash script written in 2018 by Justin Cano

# Set the framerate value here
framerate_value=9.43

# See ffmpeg documentation to understand well all the options 
# We have first the folder frames/ in the path and we are seeking for images-xxxx.png patern within this folder.
ffmpeg -framerate $framerate_value -i frames/image-%04d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p -vf scale=1280:-2 xy_traj.mp4 

#Then, the folder frames2/ and we are seeking for images-xxxx.png patern within this folder.
ffmpeg -framerate $framerate_value -i frames2/image-%04d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p -vf scale=1280:-2 errors.mp4 
