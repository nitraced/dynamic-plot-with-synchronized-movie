%% Data handling section

% Close all figures and clear all
close all
clear all

% Load the data set
load('demoData.mat')

% To remove if we are not on an Unix machine such as Linux or Mac OS
% This part of script is done to remove old frames
!rm -f frames/* frames2/*

%% Plotting section

% a is the first figure representint the two coarses
a=figure('visible','off')
% We do not want the figure to be plotted in a window but directly exported
% to a file
set(gcf,'Visible','off')

% Classical command to hold the plot and make a grid (a grided plot seems
% more professionnal!)
hold on
grid on
% Initialisation of the coordinate of the first plot
x1 = true.x(1);
y1 = true.y(1);
% Idem with the second one
x2 = est.x(1);
y2 = est.y(1);
% Links the datasources and plot handles
h1 = plot(x1,y1,'.','XDataSource','x1','YDataSource','y1');
h2 = plot(x2,y2,'.','XDataSource','x2','YDataSource','y2');

% Legends, axis labels and axis limits options
title('XY positions','Interpreter','latex','Fontsize',18)
legend({'$(x_{ref},y_{ref})$','$(\hat{x},\hat{y})$'},'Interpreter','latex','FontSize', 16,'Location','South')
xlabel('Y axis [m]','Interpreter','latex','FontSize', 14)
ylabel('X axis [m]','Interpreter','latex','FontSize', 14)
xlim([-4.3 4.7])
ylim([-4.5 4.5])

% b is the plot of the errors the principle is similar to the previous
% lines for a
b=figure('visible','off')
set(gcf,'Visible','off')
hold on
grid on
x3 = time(1);
y3 = est.errd(1);
h3 = plot(x3,y3,'-','XDataSource','x3','YDataSource','y3');
title('Error between vicon and estimates','Interpreter','latex','Fontsize',18)
legend({'Euclidian distance'},'Interpreter','latex','FontSize', 16,'Location','Northeast')
xlabel('Time [s]','Interpreter','latex','FontSize', 16)
ylabel('Distance error [m]','Interpreter','latex','FontSize', 16)

% The frame counter is initialized
framenum = 0;

% We will plot a new figure each new 20 points
Nframe = 20; 
% 9.43 fps is the rate of our ploting value each frames represents a
% period lasting 1/9.43 seconds (good to know to assembly our .jpeg images
% in a .mp4 video) : take care to modify this value in convert2mp4.sh if
% the downsampling rate or the sampling frequence are different.

for i=Nframe:Nframe:length(true.x)
    % The data set is the accumulation of the points from the begining to
    % the current point
    x1 = true.x(1:i);
    y1 = true.y(1:i);
    x2 = est.x(1:i);
    y2 = est.y(1:i);
    % Refreshing the two dataset of the plot
    refreshdata(h1,'caller')
    refreshdata(h2,'caller')
    % Idem for h3
    x3 = time(1:i);
    y3 = est.errd(1:i);
    refreshdata(h3,'caller')
    
    % The name of our first figure will be... image-i.jpeg with four digits
    % (padded with zeros) to facilitate the .mp4 assembly, it will be
    % stored in the frames/ folder
    name = strcat('frames/image-',sprintf('%04d',framenum));
    % Export as png with the previous name
    saveas(a,name,'png');
    
    % Idem, the frame directory is now frames2
    name = strcat('frames2/image-',sprintf('%04d',framenum));
    saveas(b,name,'png');
    % Counter increment
    framenum= framenum + 1;
end

% This may be long... so a good warning is welcomed here...
warning('Print finished! You can assemble your mp4 movie now')