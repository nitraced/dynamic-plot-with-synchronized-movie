# Tutorial to put video side by side with Matlab, Kdenlive and... a bash script on Linux

*By Justin Cano, MRASL Lab, Electrical engineering department, Polytechnique Montréal, Canada.*

## Tutorial's aim

The goal of this tutorial is to make a movie with simultaneous data plot and camera video, the file video_final.mp4 is the expectated result of those lines...

The tutorial with images is provided on the .pdf version only (generated with Typora Markdown editor)

## Contents

[TOC]



## Requirements

1. MATLAB : you don't need a very advanced version, all you need is basic features such as plotting and exporting your plot as .jpg... this can be done on Python also using matplotlib.

2. ffmpeg [Cross platform and freeware] : A common package in Linux distributions providing mpeg conversion features. If you don't have it you can install it here : https://www.ffmpeg.org/

3. Kdenlive  [Cross platform and freeware]  : A free video editor with good features, can be downloadable here https://kdenlive.org

   Our examples were tested with Ubuntu 16.04 and 18.04.

### Print figures on Matlab (Python version soon)

<u>All resource for this part is located on the data/ folder in this repo.</u> 

Open **Matlab** and run the script movieplot.m 

First, we have to load our demoData.mat dataset witch represents a robot coarse estimated and accurate for a part, and the error between the two coarses.  

The script seems to be long to run: I reassure you, it's normal, do not worry. Indeed, hundreds of frames are computed then saved into .jpg files in frames/ and frames2/ folders. During this huge span of time, you can read the **Matlab** script wich is commented for understanding all of the subtleties of the art of dynamic plotting in **Matlab**. 

When the warning message indicating that the computation is over, it's time to create the movie. Have a look inside frames/ and frames2/ directories before. As Lumière Brothers in the 1890's you have the frames of your movie: let's assemble this files together.

### Assemble figures

<u>All resource for this part is located on the data/ folder in this repo.</u></u> 

Once the previous step is done, you can run convert2mp4.sh bash script  (on Linux, but the options of ffmpeg on Windows should be the same). Essentially we have two lines of codes like this one :

`ffmpeg -framerate $framerate_value -i frames/image-%04d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p -vf scale=1280:-2 xy_traj.mp4` 

We invoke **ffmpeg** software, with a given framerate. The framerate is very important to set up to gain the effect of simultaneity between the video capture and the dataset. Pay attention to your measurement/estimates sampling rate and choose a N downsampling factor (we plot N points per a frame) to gain both realistic effect and fast computing.

The next options invoked is -i for "input" the image input is located on the frames/ directory and the name template given in the Matlab script is composed by the word "image-" completed with 4 digits (in the order) and then with the extention ".png".

The -c:v argument is the video codec choosen for MPEG compression. And the next ones are for rendering options, including size and resolutions. The last one is output file. 

After a while, the figures located in frames/ leads to xy_traj.mp4 and frames2/ to errors.mp4 

![Console output](assets/1548256802815.png)

### Kdenlive project

<u>All resource for this part is located on the video/ folder in this repo.</u>

Once Kdenlive is installed, you can open the simultaneous.kdenlive project on your computer.

First we will gather all the clips needed for our litle movie...

We have both the dynamic plots present in the video/ folder added to the project. Like our clip.mp4, picked from one of my tests at the ISAE (Toulouse, France), we can see the robot moving on the movie. And now our goal is to superpose the three videos.

![Video clips loaded in Kdenlive](assets/1548258680053.png){width=250px}

#### Effects needed

For a newbie (such as me...) is a little bit hard to find the good effect to superpose two or more videos. In Kdenlive the clips layers are vertical (the first is over the second and etc...) pay attention to put the background video in the last video clip layer then.  Once the clips are loaded a simple drag and drop on the timeline can esupperposeasily made it feasible... 

![Video clips on the timeline](assets/1548259376020.png){width=270px}

We need to reduce the size of video clips errors.mp4 and xy_traj. For this purpose, we can select **position and zoom** Kdenlive effect, first make sure that **effects** view is enabled :

![Wiew button](assets/1548259655840.png){width=50px}

Normally you can see this panel afterwards and you can pick **position and zoom option** pay attention to select **Effects** tab at the bottom of this wiew.

Then select the "video 2" clip (for example) to add this effect. The effect is modifiable by enabling the view **properties** :

![Effect view alongside properties view](/home/jcano/.config/Typora/typora-user-images/1548259945336.png){width=400px}

Then you can adjust the parameters, graphically on **project monitor view** or manually on this view.

#### None magical formula exists to synchronize but a good film director!

Then, we can see that the main difficulty resides in the synchronization of dynamics plots with the movie (because dynamic plots are based on the same timestamp). We have to have a common "landmark" to synchronise the two (as we have the same basis ), in my case the synchronisation is done when the robot is turning for the second time (we see the blue "true trajectory" of the robot changing coarse and naturally the robot in the video) the work is a little bit iterative and requires patience using the project visualisation tool.

![Overview of the project with visualisation tool](assets/1548259147677.png){width=400px}

Once you found your "landmark" you can cut the clip.mp4 movie to fit to the estimates dynamical plots by selecting the edges of the videoclip 4.

#### How to add a title on Kdenlive?

By selecting **project** and then **add a title clip** the clip will appear alongside the other in the **project bin view** then you will be able to edit it by double clicking on it (and opening the title editor window). 

![Title editor tool.](assets/1548260195039.png){width=400px}

#### How to produce my video?

Such a good question : click on "render" and then follow the instructions to have the output of your dreams :)

![Render Button](assets/1548260287389.png){width=50px}

After a while, you will be able to see your output at the right place.

Thanks for reading. For any issues, please report it on this gitlab project, I will try to answer afterwards.